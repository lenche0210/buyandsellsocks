﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuyOrSellSocks.Models
{
    public class UserManager : UserManager<IdentityUser>
    {
        public UserManager() : base(new UserStore())
        {
        }
    }
}