﻿using System.Web;
using System.Web.Http;

namespace BuyOrSellSocks
{
    public class FilterConfig
    {

        public static void Configure(HttpConfiguration config)
        {
            config.Filters.Add(new AuthorizeAttribute());
        }
    }
}
