﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using BuyOrSellSocks.Models;
using System.Data.Entity;

namespace BuyOrSellSocks.Core
{
    public class UserContext : IdentityDbContext
    {
        public DbSet<UserStore> UserS { get; set; }
        public DbSet<UserManager> User { get; set; }
    }
    public class Configuration : DbMigrationsConfiguration<UserContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }
    }
}