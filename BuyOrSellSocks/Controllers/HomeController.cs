﻿using BuyOrSellSocks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuyOrSellSocks.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpPost]
        public ActionResult Index(Users user)
        {
            var Boss = BossLogin().Where(m => m.Username == user.Username && m.Password == user.Password).FirstOrDefault();
            var Admin = AdminLogin().Where(m => m.Username == user.Username && m.Password == user.Password).FirstOrDefault();
            if (Boss != null)
            {
                ViewBag.Status = "Wellcome Boss";
            }
            else if(Admin != null)
                        {
                ViewBag.Status = "Welcome Admin";
            }
            else
            {
                ViewBag.Status = "Login Failed";
            }
            return View(user);
        }
        public List<Users> BossLogin()
        {
            List<Users> Boss = new List<Users>();
            Boss.Add(new Users { Username = "user1", Password = "password1" });
            Boss.Add(new Users { Username = "user2", Password = "password2" });
            Boss.Add(new Users { Username = "user3", Password = "password3" });
            Boss.Add(new Users { Username = "user4", Password = "password4" });
            Boss.Add(new Users { Username = "user5", Password = "password5" });
            return Boss;
        }
        public List<Users> AdminLogin()
        {
            List<Users> Admin = new List<Users>();
            Admin.Add(new Users { Username = "admin", Password = "TheBoss" });
            return Admin;
        }
    }
}
      
